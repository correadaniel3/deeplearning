# Autores
# Daniel Correa Arango
# Leon David Osorio Tobon
# Sebastian Rios Ruiz
# Brayan Suarez Montoya

# Redes Neuronales Artificales Profundas Entrenamiento

# Importar Librerias Keras para Deep Learning
from keras import backend as K
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.utils import np_utils
from keras.datasets import mnist

# Importar numpy para operaciones con matrices y vectores y mas
import numpy as np

K.set_image_dim_ordering('th')

np.random.seed(123) # Semilla de variabilidad en la seleccion de los datos para la validacion cruzada

# Carga de la base de datos (NMIST) en conjuntos de entrenamiento y verificacion
(X_train, y_train), (X_test, y_test) = mnist.load_data()

# Preprocesamiento de los datos, tamanio 28px X 28px y formato de arreglo Numpy
X_train = X_train.reshape(X_train.shape[0], 1, 28, 28)
X_test = X_test.reshape(X_test.shape[0], 1, 28, 28)
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_train /= 255
X_test /= 255

# Etiquetas de las diez clases (numeros del 0 al 9)
Y_train = np_utils.to_categorical(y_train, 10)
Y_test = np_utils.to_categorical(y_test, 10)

# Se crea y define arquitectura del modelo, en este caso Redes Neuronales Artificiales Profundas
model = Sequential()

# Se agregan las capas ocultas de la red neuronal, definiendo funcion de activacion, cantidad de neuronas y formato de las entradas
model.add(Convolution2D(32, 3, 3, activation='relu', input_shape=(1, 28, 28)))
model.add(Convolution2D(32, 3, 3, activation='relu'))

# Procesamiento de los datos de la base de datos NMIST y prevencion de sobreajuste en el entrenamiento
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(10, activation='softmax'))


# Compilacion del modelo
model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

# Evaluacion y entrenamiento con validacion cruzada con el modelo y los datos
model.fit(X_train, Y_train,
          batch_size=32, nb_epoch=10, verbose=1)

# Evaluacion con los datos de validacion
score = model.evaluate(X_test, Y_test, verbose=0)

# Serializacion del modelo para guardarlo luego del entrenamiento en formato JSON
model_json=model.to_json()
with open('modelo.json', 'w') as json_file:
    json_file.write(model_json)

# Serializacion de los pesos para guardarlos luego del entrenamiento en formato h5
model.save_weights("model.h5")
print ("Model save to disk")

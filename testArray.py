# Autores
# Daniel Correa Arango
# Leon David Osorio Tobon
# Sebastian Rios Ruiz
# Brayan Suarez Montoya

# Redes Neuronales Artificales Profundas Validacion

# Importar PIL para procesamiento de la imagen de entrada
import PIL
from PIL import Image
from PIL import ImageOps

# Importar numpy para operaciones con matrices y vectores y mas
import numpy as np

# Importar Librerias Keras para Deep Learning
from keras.models import model_from_json

# Carga del modelo guardado luego del entrenamiento
json_file=open("modelo.json","r")
loaded_model_json=json_file.read()
json_file.close()
loaded_model=model_from_json(loaded_model_json)

# Carga de los pesos del modelo luego del entrenamiento
loaded_model.load_weights("model.h5")

# Para contar los valores correctos
corrects = 0

for i in range(20):

    # Abrir el archivo que se desea procesar
    img=Image.open('imagenes/'+str(i)+'.jpg')

    # Conversion a RGB
    img=img.convert('RGB')

    # Invertir colores de la imagen porque las muestras de entrenamiento lo requieren
    inverted_image = PIL.ImageOps.invert(img)

    # Guardar imagen invertida
    inverted_image.save("inverted.jpg")

    # Cambiar tamanio de la imagen porque las muestras de entrenamiento lo requieren
    inverted_image = inverted_image.resize((28,28),PIL.Image.ANTIALIAS)

    # Guardar imagen redimensionada
    inverted_image.save("inverted1.jpg")

    # Convertir la imagen a un arreglo de Numpy y adecuar la matriz de entrada
    x=np.asarray(inverted_image)
    x=x.transpose(2,0,1)
    x=np.expand_dims(x,axis=1)

    # Prediccion utilizando el modelo entrenado
    testeo = loaded_model.predict(x)
    if (i%10)==np.argmax(testeo):
        corrects = corrects + 1
    print ("El numero de la imagen real es: ",i," El numero predecido es: ",np.argmax(testeo))
print ("La cantidad de predicciones correctas es: ",corrects)
